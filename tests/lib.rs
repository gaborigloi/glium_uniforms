extern crate glium_uniforms;

#[macro_use]
extern crate glium;

use glium_uniforms::*;

#[test]
fn combine_uniforms_storage_and_array() {
    use std::collections::HashSet;
    use glium::uniforms::{ Uniforms, UniformValue };

    let mut expected = HashSet::new();
    expected.insert(("a".to_owned(), 1_u32));
    expected.insert(("b".to_owned(), 2));
    expected.insert(("c".to_owned(), 3));
    expected.insert(("d".to_owned(), 4));

    let uniforms = uniform! {
        a: 1_u32,
        b: 2_u32
    };
    let uniforms_array = [("c", 3_u32), ("d", 4_u32)];
    let uniforms = CombinedUniforms::new(uniforms, UniformsArray::new(&uniforms_array));

    let mut visited = HashSet::new();
    uniforms.visit_values(|name, value| {
        match value {
            UniformValue::UnsignedInt(n) => {
                visited.insert((name.to_owned(), n));
            }
            _ => panic!("unexpected value")
        }
    });
    assert_eq!(expected, visited);
}